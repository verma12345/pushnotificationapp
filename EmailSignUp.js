import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  Image,
  TouchableOpacity,
  Dimensions,
  ScrollView,
  TextInput,
  ActivityIndicator,
} from 'react-native';

import { StackActions } from '@react-navigation/native';


class EmailSignUp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      first_name: '',
      last_name: '',
      email: '',
      password: '',
      confirm_pass: '',

      is_loading: false,

    }
    this.fcm = ''
  }


  componentDidMount() {

    // this.testSchedule()
    PushNotification.configure({
      // (optional) Called when Token is generated (iOS and Android)
      onRegister: function (token) {
        console.log("TOKEN:", token);
        this.fcm = token
      },
      // (required) Called when a remote is received or opened, or local notification is opened
      onNotification: function (notification) {
        console.log("NOTIFICATION:", notification);
        alert(notification.title)
        // console.log(notification);
        // process the notification
        // (required) Called when a remote is received or opened, or local notification is opened
        // notification.finish(PushNotificationIOS.FetchResult.NoData);
      },
      // (optional) Called when Registered Action is pressed and invokeApp is false, if true onNotification will be called (Android)
      onAction: function (notification) {
        console.log("ACTION:", notification.action);
        console.log("NOTIFICATION:", notification);
        // process the action
      },
      // (optional) Called when the user fails to register for remote notifications. Typically occurs when APNS is having issues, or the device is a simulator. (iOS)
      onRegistrationError: function (err) {
        console.error(err.message, err);
      },
      permissions: {
        alert: true,
        badge: true,
        sound: true,
      },
      // Should the initial notification be popped automatically
      // default: true
      popInitialNotification: true,
      requestPermission: true,
    });
  }

  _onClickSignUp = () => {

    if (
      this.state.first_name == '' ||
      this.state.last_name == '' ||
      this.state.email == '' ||
      this.state.password == '' ||
      this.state.confirm_pass == '') {
      alert('All field must be filled out!');
      return;
    }



    if (this.state.password != this.state.confirm_pass) {
      alert('Your password and confirmation password do not match.');
      return;
    }

    let param = {
      workout: {
        first_name: this.state.first_name,
        last_name: this.state.last_name,
        email: this.state.email,
        password: this.state.password,
        fcm_id: this.fcm.token
        // fcm_id: 'etOs_V20SjOyKQQuJpzs53:APA91bHIEAScOdPGtsT2lli8ifvXpWc_rMny5cNe6XaoWXB2-7Xl0DDluroEqRmfz-WdQ2m-wjxeJbHA4VL3bwNv2m9Ur7qVQUJ0czDrlh-3uLRIwjd3oY3lzTIrBOZGe2VLbKEcE4-J'
        // device_os
        // version_number
      },
    };

    this.hitSignupAPI(param).then((response) => {
      let _response = response.workout;

      console.log("RESPONSE::", _response);
      if (_response.status == 1) {
        this.props.navigation.dispatch(
          StackActions.replace('PushNotificationScreen'),
        )
      } else {
        alert(_response.msg);
      }
    });
  };



  hitSignupAPI = async (param) => {
    this.setState({ is_loading: !this.state.is_loading })
    let data = await fetch(`https://demoapps.in/workout/index.php/User_controller/sign_up`, {
      method: 'post',
      body: JSON.stringify(param),
    });
    this.setState({ is_loading: !this.state.is_loading })
    let response = await data.json();
    return response
  }


  render() {
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.mainViewStyle}>

          <ScrollView
            contentContainerStyle={{ flex: 1 }}
          >

            <TextInput
              placeholder="First Name"
              value={this.state.first_name}
              // keyboardType="name"
              maxLength={15}
              onChangeText={(text) => {
                this.setState({ first_name: text })
              }}
              style={[styles.inputStyle, { marginTop: 50 }]}
            />

            <TextInput
              placeholder="Last Name"
              value={this.state.last_name}
              // keyboardType="name" 
              maxLength={15}
              onChangeText={(text) => {
                this.setState({ last_name: text })
              }}
              style={styles.inputStyle}
            />


            <TextInput
              placeholder="Email Id"
              value={this.state.email}
              keyboardType="email-address"
              maxLength={35}
              onChangeText={(text) => {
                this.setState({ email: text })
              }}
              style={styles.inputStyle}
            />

            <TextInput
              placeholder="Enter password"
              value={this.state.password}
              maxLength={15}
              onChangeText={(text) => {
                this.setState({ password: text })
              }}
              style={styles.inputStyle}
            />

            <TextInput
              placeholder="Confirm password"
              value={this.state.confirm_pass}
              maxLength={15}
              onChangeText={(text) => {
                this.setState({ confirm_pass: text })
              }}
              style={styles.inputStyle}
            />

            <TouchableOpacity
              onPress={() => { this._onClickSignUp() }}
              style={[styles.btnStyle]} >
              <Text>
                {"Sign Up"}
              </Text>
              {this.state.is_loading ?
                <ActivityIndicator
                  size='small'
                  color='red'
                />
                : null
              }
            </TouchableOpacity>

          </ScrollView>

        </View>
      </SafeAreaView>
    );
  }
}

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  inputStyle: {
    marginHorizontal: 20,
    backgroundColor: 'white',
    marginVertical: 10,
    borderRadius: 10,
    elevation: 6,
    paddingHorizontal: 10
  },
  btnStyle: {
    alignSelf: 'center',
    marginVertical: 50,
    backgroundColor: 'lightyellow',
    elevation: 6,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 100,
    borderRadius: 20,
    flexDirection: 'row'
  },

  mainViewStyle: {
    flex: 1,
  },
});

export default EmailSignUp;
