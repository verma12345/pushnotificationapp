import React from 'react'
import { Component } from "react";
import { Button, StyleSheet, Text, TextInput, View, TouchableOpacity, } from 'react-native';
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import PushNotificationScreen from './PushNotificationScreen';
import EmailSignUp from './EmailSignUp';
import LazyLoadingScreen from './src/screens/LazyLoadingScreen';
import HomeScreen from './src/screens/HomeScreen';
import MaskViewScreen from './src/screens/MaskViewScreen';
import VideoPlayerScreen from './src/screens/VideoPlayerScreen';
import TestVideoPlayer from './src/screens/TestVideoPlayer';
import VideoRecordingScreen from './src/screens/VideoRecordingScreen';
import GridViewScreen from './src/screens/GridViewScreen';
import WeatherForcasting from './src/screens/WeatherForcasting';
import SocketApiScreen from './src/screens/SocketApiScreen';
import OpenFile from './src/screens/OpenFile';



class App extends Component {
  constructor(props) {
    super(props)
  }

  Stack = createStackNavigator()

  render() {
    return (
      <View style={styles.container}>
        <NavigationContainer  >
          <this.Stack.Navigator screenOptions={{ headerShown: false }} >

            {/* <this.Stack.Screen name='EmailSignUp' component={EmailSignUp} /> */}
            {/* <this.Stack.Screen name='PushNotificationScreen' component={PushNotificationScreen} /> */}

            <this.Stack.Screen name='HomeScreen' component={HomeScreen} />

            <this.Stack.Screen name='LazyLoadingScreen' component={LazyLoadingScreen} />

            <this.Stack.Screen name='MaskViewScreen' component={MaskViewScreen} />
            <this.Stack.Screen name='VideoPlayerScreen' component={VideoPlayerScreen} />
            <this.Stack.Screen name='TestVideoPlayer' component={TestVideoPlayer} />
            <this.Stack.Screen name='VideoRecordingScreen' component={VideoRecordingScreen} />
            <this.Stack.Screen name='WeatherForcasting' component={WeatherForcasting} />
            <this.Stack.Screen name='GridViewScreen' component={GridViewScreen} />
            <this.Stack.Screen name='SocketApiScreen' component={SocketApiScreen} />

            <this.Stack.Screen name='OpenFile' component={OpenFile} />
            
          </this.Stack.Navigator>
        </NavigationContainer>
      </View>
    );
  }
}
const styles = StyleSheet.create({

  container: {
    flex: 1,
    backgroundColor: '#F7BE16'
  },

})
export default App;