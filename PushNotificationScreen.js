import React from 'react'
import { Component } from "react";
import { Button, StyleSheet, Text, TextInput, View, TouchableOpacity, } from 'react-native';
import messaging, { firebase } from '@react-native-firebase/messaging';
import { enableScreens } from 'react-native-screens';
import PushNotification, { Importance } from 'react-native-push-notification';
// import firebase from '@react-native-firebase/app';
class PushNotificationScreen extends Component {
  // componentDidMount() {
  //     messaging().setBackgroundMessageHandler(async remoteMessage => {
  //         console.log('Message handled in the background!', remoteMessage);
  //     });
  // }
  componentDidMount() {

    // this.testSchedule()
    PushNotification.configure({
      // (optional) Called when Token is generated (iOS and Android)
      onRegister: function (token) {
        console.log("TOKEN:", token);
      },
      // (required) Called when a remote is received or opened, or local notification is opened
      onNotification: function (notification) {
        console.log("NOTIFICATION:", notification);
        alert(notification.title)
        // console.log(notification);
        // process the notification
        // (required) Called when a remote is received or opened, or local notification is opened
        // notification.finish(PushNotificationIOS.FetchResult.NoData);
      },
      // (optional) Called when Registered Action is pressed and invokeApp is false, if true onNotification will be called (Android)
      onAction: function (notification) {
        console.log("ACTION:", notification.action);
        console.log("NOTIFICATION:", notification);
        // process the action
      },
      // (optional) Called when the user fails to register for remote notifications. Typically occurs when APNS is having issues, or the device is a simulator. (iOS)
      onRegistrationError: function (err) {
        console.error(err.message, err);
      },
      permissions: {
        alert: true,
        badge: true,
        sound: true,
      },
      // Should the initial notification be popped automatically
      // default: true
      popInitialNotification: true,
      requestPermission: true,
    });
  }

  SendNotifiocation = () => {
    PushNotification.localNotification({
      title: "PushNotification",
      message: "This is from push notification",
      picture: 'https://www.cleverfiles.com/howto/wp-content/uploads/2018/03/minion.jpg', // (optional) Display an picture with the notification, alias of `bigPictureUrl` for Android. default: undefined
      userInfo: {}, // (optional) default: {} (using null throws a JSON value '<null>' error)
      playSound: true, // (optional) default: true
      soundName: "default", // (optional) Sound to play when the notification is shown. Value of 'default' plays the default sound. It can be set to a custom sound such as 'android.resource://com.xyz/raw/my_sound'. It will look for the 'my_sound' audio file in 'res/raw' directory and play it. default: 'default' (default sound is played)
      number: 10, // (optional) Valid 32 bit integer specified as string. default: none (Cannot be zero)
      repeatType: "day",
      actions: ["Yes", "No"],

      channelId: "jazzz_tech", // (required) channelId, if the channel doesn't exist, notification will not trigger.
      ticker: "My Notification Ticker", // (optional)
      showWhen: true, // (optional) default: true
      autoCancel: true, // (optional) default: true
      // largeIcon: "ic_launcher", // (optional) default: "ic_launcher". Use "" for no large icon.
      // largeIconUrl: "https://www.example.tld/picture.jpg", // (optional) default: undefined
      // smallIcon: "ic_notification", // (optional) default: "ic_notification" with fallback for "ic_launcher". Use "" for default small icon.
      // bigText: "My big text that will be shown when notification is expanded. Styling can be done using HTML tags(see android docs for details)", // (optional) default: "message" prop
      // subText: "This is a subText", // (optional) default: none
      // bigPictureUrl: "http://www.personal.psu.edu/kbl5192/jpg.jpg", // (optional) default: undefined
      // bigLargeIcon: "ic_launcher", // (optional) default: undefined
      // bigLargeIconUrl: "https://www.example.tld/bigicon.jpg", // (optional) default: undefined
      // color: "red", // (optional) default: system default
      // vibrate: true, // (optional) default: true
      // vibration: 300, // vibration length in milliseconds, ignored if vibrate=false, default: 1000
      // tag: "some_tag", // (optional) add tag to message
      // group: "group", // (optional) add group to message
      // groupSummary: false, // (optional) set this notification to be the group summary for a group of notifications, default: false
      // ongoing: false, // (optional) set whether this is an "ongoing" notification
      // priority: "high", // (optional) set notification priority, default: high
      // visibility: "private", // (optional) set notification visibility, default: private
      // ignoreInForeground: false, // (optional) if true, the notification will not be visible when the app is in the foreground (useful for parity with how iOS notifications appear). should be used in combine with `com.dieam.reactnativepushnotification.notification_foreground` setting
      // shortcutId: "shortcut-id", // (optional) If this notification is duplicative of a Launcher shortcut, sets the id of the shortcut, in case the Launcher wants to hide the shortcut, default undefined
      // onlyAlertOnce: false, // (optional) alert will open only once with sound and notify, default: false

      // when: null, // (optional) Add a timestamp (Unix timestamp value in milliseconds) pertaining to the notification (usually the time the event occurred). For apps targeting Build.VERSION_CODES.N and above, this time is not shown anymore by default and must be opted into by using `showWhen`, default: null.
      // usesChronometer: false, // (optional) Show the `when` field as a stopwatch. Instead of presenting `when` as a timestamp, the notification will show an automatically updating display of the minutes and seconds since when. Useful when showing an elapsed time (like an ongoing phone call), default: false.
      // timeoutAfter: null, // (optional) Specifies a duration in milliseconds after which this notification should be canceled, if it is not already canceled, default: null

      // messageId: "google:message_id", // (optional) added as `message_id` to intent extras so opening push notification can find data stored by @react-native-firebase/messaging module. 

      // actions: ["Yes", "No"], // (Android only) See the doc for notification actions to know more
      // invokeApp: true, // (optional) This enable click on actions to bring back the application to foreground or stay in background, default: true

      // /* iOS only properties */
      // category: "", // (optional) default: empty string
      // subtitle: "My Notification Subtitle", // (optional) smaller title below notification title

      // /* iOS and Android properties */
      // id: 0, // (optional) Valid unique 32 bit integer specified as string. default: Autogenerated Unique ID
      // title: "My Notification Title", // (optional)
      // message: "My Notification Message", // (required)
      // picture: "https://www.example.tld/picture.jpg", // (optional) Display an picture with the notification, alias of `bigPictureUrl` for Android. default: undefined
      // userInfo: {}, // (optional) default: {} (using null throws a JSON value '<null>' error)
      // playSound: false, // (optional) default: true
      // soundName: "default", // (optional) Sound to play when the notification is shown. Value of 'default' plays the default sound. It can be set to a custom sound such as 'android.resource://com.xyz/raw/my_sound'. It will look for the 'my_sound' audio file in 'res/raw' directory and play it. default: 'default' (default sound is played)
      // number: 10, // (optional) Valid 32 bit integer specified as string. default: none (Cannot be zero)
      // repeatType: "day", // (optional) Repeating interval. Check 'Repeating Notifications' section for more info.
    })
  }

  cancelNotificationAll = () => {
    PushNotification.cancelAllLocalNotifications()
  }

  cancelNotificationById = () => {
    PushNotification.cancelAllLocalNotifications('-339208834')
  }

  getNotificationAll = () => {
    PushNotification.getScheduledLocalNotifications(callback => {
      console.log('getScheduledLocalNotifications:>>', callback);
    });
  }

  testSchedule = () => {
    PushNotification.localNotificationSchedule({
      messsage: 'My Notification Msg',
      date: new Date(Date.now() + 60 * 1000), // in 60 secs
    })
  }

  async checkPermission() {
    console.log("check permission fxn call")
    const enabled = await messaging().hasPermission();
    console.log("check permission fxn call enable ", enableScreens)
    if (enabled) {
      this.getToken();
    } else {
      this.requestPermission();
    }
  }
  //   remot notification token
  async getToken() {
    console.log("get token call");
    // let fcmToken = await AsyncStorage.read('fcmToken');
    // console.log("get token call fcm token ", fcmToken);
    // if (!fcmToken) {
    const fcmToken = await messaging().getToken();
    // if (fcmToken) {
    console.log("check fcm token ", fcmToken);
    // await AsyncStorage.save('fcmToken', fcmToken);
    // }
    // }
  }
  // remote location msg user permission
  async requestPermission() {
    console.log("requestPermisiion call ");
    try {
      await messaging().requestPermission();
      // user has authorised
      this.getToken();
    } catch (error) {
      // user has rejected permission
    }
  }

  getChannel = () => {
    PushNotification.getChannels(function (channel_ids) {
      console.log(channel_ids); // ['channel_id_1']
    });
  }

  creteChannel1 = () => {
    PushNotification.createChannel(
      {
        channelId: "fcm_fallback_notification_channel", // (required)
        channelName: "My channel", // (required)
        channelDescription: "A channel to categorise your notifications", // (optional) default: undefined.
        playSound: false, // (optional) default: true
        soundName: "default", // (optional) See `soundName` parameter of `localNotification` function
        importance: Importance.HIGH, // (optional) default: Importance.HIGH. Int value of the Android notification importance
        vibrate: true, // (optional) default: true. Creates the default vibration pattern if true.
      },
      (created) => console.log(`createChannel returned '${created}'`) // (optional) callback returns whether the channel was created, false means it already existed.
    );
  }

  deleteChannel1 = () => {
    PushNotification.deleteChannel('fcm_fallback_notification_channel');
  }

  render() {
    return (
      <View style={{ flex: 1 }}>

        <Text style={{ fontSize: 20, backgroundColor: '#1B95a0', padding: 10, color: 'white', fontWeight: '700' }} >{'Firebase Push Notification'} </Text>
        <View style={{ flex: 1, alignItems: 'center' }} >
          <TouchableOpacity
            onPress={() => { this.SendNotifiocation() }}
            style={[styles.button]}
          >
            <Text style={[styles.text]} >{'Send Notification'} </Text>

          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => { this.getNotificationAll() }}
            style={[styles.button]}
          >
            <Text style={[styles.text]} >{'Get Notification'} </Text>

          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => { this.cancelNotificationAll() }}
            style={[styles.button]}
          >
            <Text style={[styles.text]} >{'Cancel Notification'} </Text>

          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => { this.cancelNotificationById() }}
            style={[styles.button]}
          >
            <Text style={[styles.text]} >{'Cancel By ID Notification'} </Text>

          </TouchableOpacity>


          <TouchableOpacity
            onPress={() => { this.getChannel() }}
            style={[styles.button]}
          >
            <Text style={[styles.text]} >{'Get Channel'} </Text>

          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => { this.creteChannel1() }}
            style={[styles.button]}
          >
            <Text style={[styles.text]} >{'Create Channel'} </Text>

          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => { this.deleteChannel1() }}
            style={[styles.button]}
          >
            <Text style={[styles.text]} >{'Delete Channel'} </Text>

          </TouchableOpacity>
        </View>

      </View>
    );
  }
}
const styles = StyleSheet.create({
  button: {
    backgroundColor: '#1B95a0',
    width: '90%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20,
    elevation: 10,
    marginTop: 20
  },
  container: {
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    padding: 32,
    backgroundColor: '#F7BE16'
  },
  text: {
    textAlign: 'justify',
    fontSize: 18,
    color: 'white'
  },
  text1: {
    textAlign: 'justify',
    paddingVertical: 32,
    fontSize: 20,
    color: 'white'
  },
  enappd:
  {
    textDecorationLine: 'underline',
    fontWeight: 'bold',
    color: 'green'
  }
})
export default PushNotificationScreen;