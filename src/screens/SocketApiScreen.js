import React, { useEffect, useState } from 'react'
import { Component } from "react";
import {
  StyleSheet, Text, TextInput, TouchableOpacity, View,
  Button, ScrollView, FlatList, PermissionsAndroid,
  Image,
  Dimensions,
  StatusBar,
  ImageBackground,
  ActivityIndicator,
  Platform
} from "react-native";
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import io from "socket.io-client";
import DocumentPicker from 'react-native-document-picker';
import FileViewer from 'react-native-file-viewer';
// import filesize from 'filesize'
import RNFetchBlob from 'rn-fetch-blob';
import ChatRow from '../row_component/ChatRow';


const today = new Date();
const date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
const time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();

const SocketApiTesting = (props) => {
  const [chatMessage, setMessage] = useState('')
  const [chatMessages, setMessages] = useState('')
  const [id, setId] = useState(1)
  const [socket, setSocket] = useState(io('http://192.168.0.186:3000/'))
  const [filePath, setFilePath] = useState(null);
  const [image, setImage] = useState('');

  const [isPopup, setPopup] = useState(false);
  const [isImagePopup, setImagePopup] = useState(false);
  const [isLoading, setLoading] = useState(true);
  const [multipleFile, setMultipleFile] = useState([]);

  const [document, setDocument] = useState('');
  const [imagePreview, setImagePreview] = useState('');




  const toggleImagePopup = () => setImagePopup(!isImagePopup);


  useEffect(() => {
    requestCameraPermission()
    socket.on('chat_message', (msg) => {
      setMessages(msg)
      // console.log(msg)

    });
  }, [chatMessages])

  const chooseFile = () => {
    let options = {
      // saveToPhotos: true,
      mediaType: 'mixed',
      includeBase64: true,
      // includeBase64
    };
    launchImageLibrary(options, (response) => {
      console.log('Response = ', response);
      // setUploadSuccessMessage('');
      if (response.didCancel) {
        alert('User cancelled camera picker');
        return;
      } else if (response.errorCode == 'camera_unavailable') {
        alert('Camera not available on device');
        return;
      } else if (response.errorCode == 'permission') {
        alert('Permission not satisfied');
        return;
      } else if (response.errorCode == 'others') {
        alert(response.errorMessage);
        return;
      }
      setFilePath(response);
      setImage(response.assets[0].base64)
      setPopup(!isPopup)
      setImagePreview(response.assets[0])

    });
  };

  const chooseFileFromCamera = () => {
    let options = {
      // saveToPhotos: true,
      mediaType: 'photo',
      includeBase64: true,
      cameraType: 'front'
    };
    launchCamera(options, (response) => {
      console.log('Response = ', response);
      if (response.didCancel) {
        alert('User cancelled camera picker');
        return;
      } else if (response.errorCode == 'camera_unavailable') {
        alert('Camera not available on device');
        return;
      } else if (response.errorCode == 'permission') {
        alert('Permission not satisfied');
        return;
      } else if (response.errorCode == 'others') {
        alert(response.errorMessage);
        return;
      }
      setFilePath(response);
      setImage(response.assets[0].base64)
      setPopup(!isPopup)

    });
  };

  const requestCameraPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA,

        {
          title: "Cool Photo App Camera Permission",
          message:
            "Cool Photo App needs access to your camera " +
            "so you can take awesome pictures.",
          buttonNeutral: "Ask Me Later",
          buttonNegative: "Cancel",
          buttonPositive: "OK"
        }
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log("You can use the camera");
      } else {
        console.log("Camera permission denied");
      }
    } catch (err) {
      console.warn(err);
    }
  };



  const submitChatMessage = () => {
    if (multipleFile.length) {
      var formdata = new FormData();
      // formdata.append("file_url", fileInput.files[0], "test 3.pdf");
      formdata.append("file_url", multipleFile[0], "test3.pdf");
      let type = multipleFile[0].type.split('/').slice(1)
      var requestOptions = {
        method: 'POST',
        body: formdata,
        redirect: 'follow'
      };
      if (imagePreview != '') {
        onImagePreview()
      }
      fetch("https://demoapps.in/poker/index.php/Image_controller/upload_file", requestOptions)
        .then(response => response.json())
        .then(result => {
          console.log(result.poker.file_url)
          hitSendMessage(result.poker.file_url, type[0])
        })
        .catch(error => console.log('error', error));
    } else {
      hitSendMessage('', '')
    }
  }


  const hitSendMessage = (document,type) => {

    let param = {
      message: chatMessage,
      id: id,
      image: image,
      time: time,
      document: document,
      docType: type
    }

    console.log('++++++++++++++++++++++++++++++++++++++++++++++++++++');
    console.log(param);

    socket.emit('chat_message', param);

    setFilePath(null);
    setMultipleFile([]);
    onImagePreview()
    setImage('')
    setDocument('')
  }




  const selectMultipleFile = async () => {

    try {
      const results = await DocumentPicker.pickMultiple({
        type: [DocumentPicker.types.allFiles],
      });
      console.log(results[0]);
      setMultipleFile(results)

      setImagePreview(results[0])

    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        alert('Canceled from multiple doc picker');
      } else {
        alert('Unknown Error: ' + JSON.stringify(err));
        throw err;
      }
    }
  }

  const onImagePreview = () => {
    let data = [...chatMessages, {
      id: id,
      image: '',
      time: time,
      document: imagePreview.uri,
      message: chatMessage,
    }]
    setMessages(data)
    onCancelPopUp()
  }


  const checkPermission = async (fileUrl) => {
    console.log(fileUrl);

    if (Platform.OS === 'ios') {
      downloadFile(fileUrl);
    } else {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'Storage Permission Required',
            message:
              'Application needs access to your storage to download File',
          }
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          // Start downloading
          downloadFile(fileUrl);
          console.log('Storage Permission Granted.');
        } else {
          // If permission denied then show alert
          Alert.alert('Error', 'Storage Permission Not Granted');
        }
      } catch (err) {
        // To handle permission related exception
        console.log("++++" + err);
      }
    }
  };

  const downloadFile = (fileUrl) => {

    // Get today's date to add the time suffix in filename
    let date = new Date();
    // File URL which we want to download
    let FILE_URL = fileUrl;
    // Function to get extention of the file url
    let file_ext = getFileExtention(FILE_URL);

    file_ext = '.' + file_ext[0];

    // config: To get response by passing the downloading related options
    // fs: Root directory path to download
    const { config, fs } = RNFetchBlob;
    let RootDir = fs.dirs.PictureDir;
    let options = {
      fileCache: true,
      addAndroidDownloads: {
        path:
          RootDir +
          '/file_' +
          Math.floor(date.getTime() + date.getSeconds() / 2) +
          file_ext,
        description: 'downloading file...',
        notification: true,
        // useDownloadManager works with Android only
        useDownloadManager: true,
      },
    };
    config(options)
      .fetch('GET', FILE_URL)
      .then(res => {
        // Alert after successful downloading
        console.log('res -> ', JSON.stringify(res));
        alert('File Downloaded Successfully.');
      });
  };

  const getFileExtention = fileUrl => {
    // To get the file extension
    return /[.]/.exec(fileUrl) ?
      /[^.]+$/.exec(fileUrl) : undefined;
  };



  const renderItem2 = (data) => {
    return (
      <ChatRow
        id={id}
        data_row={data.item}
        onPermission={checkPermission}

      />
    )
  }

  const onCancelPopUp = () => {
    setImagePreview('')
  }

  return (

    <View style={styles.container} >

      <View style={{ backgroundColor: '#202a33', borderRadius: 9, marginTop: 7, paddingHorizontal: 14, paddingVertical: 5, alignSelf: 'center', justifyContent: 'center' }}>
        <Text style={{ fontSize: 14, fontWeight: '700', textAlign: 'center', color: 'white' }}>{date} </Text>
      </View>

      <FlatList
        data={chatMessages}
        renderItem={renderItem2}
        keyExtractor={(item, index) => 'key' + index}
        extraData={chatMessages}
        bounces={false}
      />

      {imagePreview != '' ?
        <View
          style={{
            width: '80%', margin: 10,
            borderRadius: 20,
            elevation: 5,
            padding: 10,
            backgroundColor: '#202A33',
          }}
        >
          {
            imagePreview.type.includes('pdf') ?

              <View style={{ flexDirection: 'row', }}>
                <View style={{ paddingVertical: 5, paddingHorizontal: 2, borderRadius: 10, height: 60, width: 60, backgroundColor: 'red', alignItems: 'center' }}>
                  <Text style={{ fontSize: 18, color: 'white', }}>{imagePreview.type.split('/').slice(1)} </Text>
                </View>
                <Text style={{ fontSize: 13, color: 'white', marginLeft: 12, backgroundColor: '#1E292D', marginTop: 16 }}>{imagePreview.name} </Text>
              </View>
              : image != '' ?

                <ImageBackground
                  borderRadius={10}
                  source={{ uri: 'data:image/png;base64,' + image }}
                  style={{
                    height: 100,
                    width: 200,
                    borderColor: '#005c49',
                    justifyContent: 'flex-end',
                    alignItems: 'flex-end',
                  }}
                  imageStyle={{
                    borderWidth: 2.5,
                    borderColor: 'green'
                  }}
                >
                </ImageBackground>
                :
                
                <View style={{ flexDirection: 'row', }}>
                  <View style={{ padding:20, borderRadius: 10,  backgroundColor: imagePreview.type.includes('pdf')? 'red':'gray' }}>
                    <Text style={{ fontSize: 18, color: 'white', }}>{imagePreview.type.split('/').slice(1)} </Text>
                  </View>
                  <Text style={{ fontSize: 13, color: 'white', marginLeft: 12, backgroundColor: '#1E292D', marginTop: 16 }}>{imagePreview.name} </Text>
                </View>


          }


          <Text style={{ fontSize: 13, color: 'white', marginLeft: 12, backgroundColor: '#1E292D', marginTop: 16 }}>{chatMessage} </Text>

          <TouchableOpacity
            onPress={() => { onCancelPopUp() }}
            style={{ position: 'absolute', right: 10, top: 0, padding: 10 }} >
            <Text style={{ fontSize: 18, color: 'white', fontWeight: '700', }}>x</Text>
          </TouchableOpacity>
        </View>
        :
        null
      }


      <View style={{
        flexDirection: 'row',
        width: '95%',
        borderRadius: 100,
        alignSelf: 'center',
        alignItems: 'center',
      }}>


        <View style={{
          flexDirection: 'row',
          width: '85%',
          elevation: 10,
          backgroundColor: '#334657',
          borderRadius: 100,
          alignItems: 'center',
          marginBottom: 10,
          height: 45,
        }}>
          <TextInput
            placeholder="Message"
            style={{ flex: 1, fontSize: 18, paddingHorizontal: 10 }}
            autoCorrect={false}
            value={chatMessage}
            placeholderTextColor="white"
            color="white"
            onChangeText={chatMessage => {
              setMessage(chatMessage)
            }}
          />

          <TouchableOpacity
            style={{
              height: 40, width: 40, justifyContent: 'center',
              alignItems: 'center', borderRadius: 100
            }}
            onPress={() => setPopup(!isPopup)}>
            <Image
              style={{ height: 28, width: 28, tintColor: 'white' }}
              source={require('../../assets/share.png')}
            />
          </TouchableOpacity>
        </View>


        <TouchableOpacity
          style={{
            height: 40, width: 40, justifyContent: 'center',
            backgroundColor: '#09a587',
            alignItems: 'center', borderRadius: 100,
            marginBottom: 5, marginLeft: 10
          }}
          onPress={submitChatMessage}>
          <Image
            style={{ height: 28, width: 28, tintColor: 'white' }}
            source={require('../../assets/send.png')}
          />
        </TouchableOpacity>
      </View>

      {isPopup ?
        <View style={[{
          flexDirection: 'row',
          justifyContent: 'space-around',
          backgroundColor: '#25343b',
          paddingHorizontal: 15,
          paddingVertical: 13,
          paddingBottom: 30,
          elevation: 6,
          borderBottomWidth: 1
        }]} >

          <TouchableOpacity
            onPress={() => selectMultipleFile()}
            style={{ justifyContent: 'center', borderRadius: 100, alignItems: 'center', height: 50, width: 50, backgroundColor: '#7e65ff' }} >
            <Image
              style={{ height: 25, width: 25, tintColor: 'white' }}
              source={require('../../assets/document.png')}
            />
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => chooseFileFromCamera()}
            style={{ justifyContent: 'center', borderRadius: 100, alignItems: 'center', height: 50, width: 50, backgroundColor: '#fd2e74' }} >
            <Image
              style={{ height: 25, width: 25, tintColor: 'white' }}
              source={require('../../assets/camera.png')}
            />
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => chooseFile()}
            style={{ justifyContent: 'center', borderRadius: 100, alignItems: 'center', height: 50, width: 50, backgroundColor: '#b85ae4' }} >
            <Image
              style={{ height: 25, width: 25, tintColor: 'white' }}
              source={require('../../assets/gallery.png')}
            />
          </TouchableOpacity>
        </View>
        : null
      }



    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#25343b',
  },
});

export default SocketApiTesting;