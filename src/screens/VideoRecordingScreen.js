import React from 'react'
import { Component } from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import Footer from '../components/Footer';
import { RNCamera } from 'react-native-camera';
import Ionicons from 'react-native-vector-icons/Ionicons';


class VideoRecordingScreen extends Component {
  constructor(props) {
    super(props)
  }

  takePicture = async () => {
    try {
      const data = await this.camera.takePictureAsync();
      console.log('Path to image: ' + data.uri);
    } catch (err) {
      // console.log('err: ', err);
    }
  };

  render() {
    return (

      <View style={{ flex: 1 }}>
        <View style={{ flex: 1 }} >

        <RNCamera
          ref={cam => {
            this.camera = cam;
          }}
          style={styles.preview}
        >
          <View style={styles.captureContainer}>
            <TouchableOpacity style={styles.capture} onPress={this.takePicture}>
              {/* <Icon style={styles.iconCamera}></Icon> */}
              <Ionicons name={'camera'} style={{ fontSize: 15, color: 'white' }} />

              <Text>Take Photo</Text>
            </TouchableOpacity>
          </View>
        </RNCamera>


        </View>
        <Footer
          navigation={this.props.navigation}
        />
      </View>
    )
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  captureContainer: {
    height: 100,
    width: 300,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center'
  },
  preview: {
    height: 200,
    width: 200,
  },
  space: {
    flex: 1
  },
  capture: {
    padding: 10,
    backgroundColor: 'red'
  },
  iconCamera: {
    fontSize: 20,
    color: 'gray'
  }
})

export default VideoRecordingScreen