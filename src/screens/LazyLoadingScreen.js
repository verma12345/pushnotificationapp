import React from 'react'
import { Component } from "react";
import { Dimensions, FlatList, Image, StyleSheet, Text, View } from "react-native";
import Footer from '../components/Footer';
import LazyLoadingComponent from '../components/LazyLoadingComponent';
import ProgressiveImage from '../components/ProgressiveImage';

class LazyLoadingScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isLoading: true,
            data: [
                {
                    image: 'https://wallpapercave.com/wp/wp5586443.jpg'
                },
                {
                    image: 'https://www.desktopbackground.org/download/o/2011/07/30/242534_high-resolution-space-wallpapers-high-resolution-full-size_1920x1080_h.jpg'
                },
                {
                    image: 'https://wallpaperaccess.com/full/508751.jpg'
                },
                {
                    image: 'https://wallpaperaccess.com/full/641866.jpg'
                },
                {
                    image: 'https://images8.alphacoders.com/732/thumb-1920-732176.jpg'
                },
                {
                    image: 'https://wallpaperaccess.com/full/2887153.jpg'
                },
                {
                    image: 'http://www.hdwallpaperslife.com/wp-content/uploads/2018/07/sunset_digital_paint_4k.jpg'
                },
                {
                    image: 'https://wallpaperaccess.com/full/651314.jpg'
                },
            ]
        }
    }


    renderItem = (data) => {
        console.log(data.item.image);
        return (
            < View style={{ flexDirection: 'row', alignItems: 'center', margin: 5, justifyContent: 'center' }} >
                <ProgressiveImage
                    defaultImageSource={{uri:'https://image.shutterstock.com/image-vector/high-low-res-image-icon-260nw-678227896.jpg'}}
                    source={{ uri: data.item.image }}
                    style={{ height: width / 2, width: width - 20, borderRadius: 5 }}
                />
            </View>
        )
    }

    componentDidMount() {
        setTimeout(() => {
            this.setState({ isLoading: false })
        }, 1000)
    }

    render() {
        return (
            <View style={{ flex: 1 }} >
                <View style={{ flex: 1, marginHorizontal: 10 }} >

                    {
                        this.state.isLoading ?
                            <LazyLoadingComponent />
                            :
                            <View style={{ flex: 1, }} >
                                <FlatList
                                    contentContainerStyle={{}}
                                    data={this.state.data}
                                    renderItem={this.renderItem}
                                    // numColumns={3}
                                />
                            </View>
                    }
                </View>
                <Footer
                    navigation={this.props.navigation}
                />
            </View>
        )
    }
}
const { width, height } = Dimensions.get('window')
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    txt: {
        fontSize: 16,
        fontWeight: 'bold',
    }
})

export default LazyLoadingScreen