import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { WebView } from 'react-native-webview';

class OpenFile extends Component {
  render() {
    return <WebView source={{ uri: 'https://demoapps.in/poker/upload/profile_pic/file/9386824.pdf' }} />;
  }
}

export default OpenFile