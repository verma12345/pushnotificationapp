import React, { Component } from "react"
import { Image, StyleSheet, Text, View, SafeAreaView, ScrollView, Dimensions, FlatList } from "react-native";
import Footer from "../components/Footer";

export default class GridViewScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {

            data: [
                {
                    image: require('../../assets/college.jpeg')
                },
                {
                    image: require('../../assets/event1.png')
                },
                {
                    image: require('../../assets/event1.png')
                },
                {
                    image: require('../../assets/event1.png')
                },
                {
                    image: require('../../assets/event1.png')
                },
                {
                    image: require('../../assets/event1.png')
                },
                {
                    image: require('../../assets/event1.png')
                },
                {
                    image: require('../../assets/event1.png')
                },
            ]
        }
    }


    renderItemHeader = (data) => {
        return (

            < View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }} >

                <Image source={this.state.data.length > 0 ? this.state.data[0].image : require('../../assets/event1.png')}
                    style={{ height: width / 1.6, width: width / 1.6 }} />

                <View>
                    <Image source={this.state.data.length > 0 ? this.state.data[0].image : require('../../assets/event1.png')}
                        style={{ height: width / 3.1, width: width / 3.1 }} />

                    <Image source={this.state.data.length > 0 ? this.state.data[0].image : require('../../assets/event1.png')}
                        style={{ height: width / 3.1, width: width / 3.1 }} />
                </View>

            </View>
        )
    }


    renderItem = (data) => {
        return (
            < View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: 10, justifyContent: 'center' }} >
                {
                    data.index < 3 ?
                        data.index >= 3 ?
                            <View style={{
                                position: 'absolute', elevation: 5, backgroundColor: 'red',
                                height: width / 3.1, width: width / 3.1
                            }} >
                                <Text>
                                    {this.state.data.length}{'gggggggg'}
                                </Text>
                            </View>
                            :
                            <View  >
                                <Image source={data.item.image}
                                    style={{ height: width / 3.4, width: width / 3.4, borderRadius: 100 }} />
                                {
                                    data.index == 2 ?
                                        <View style={{
                                            position: 'absolute', backgroundColor: '#0009',
                                            height: width / 3.4, width: width / 3.4,
                                            justifyContent: 'center', alignItems: 'center',
                                            borderRadius: 100
                                        }} >
                                            <Text style={{ fontSize: 28, color: 'white', fontWeight: '700' }} >
                                                {'+'}{this.state.data.length - 6}
                                            </Text>
                                        </View>
                                        :
                                        null
                                }
                            </View>

                        :
                        null
                }

            </View>
        )
    }

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <View style={styles.container}>

                    <FlatList
                        data={this.state.data.slice(3, this.state.data.length)}
                        renderItem={this.renderItem}
                        ListHeaderComponent={this.renderItemHeader}
                        numColumns={3}
                    />
                 
                </View>
                <Footer
                        navigation={this.props.navigation}
                    />
            </SafeAreaView>
        )
    }
}

const { width, height } = Dimensions.get('window')
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    txt: {
        fontSize: 16,
        fontWeight: 'bold',
    }
})