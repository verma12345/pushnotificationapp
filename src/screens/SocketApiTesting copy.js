import React, { useEffect, useState } from 'react'
import { Component } from "react";
import {
  StyleSheet, Text, TextInput, TouchableOpacity, View,
  Button, ScrollView, FlatList, PermissionsAndroid,
  Image,
  Dimensions,
  StatusBar,
  ImageBackground,
  ActivityIndicator,
  Platform
} from "react-native";
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import io from "socket.io-client";
import DocumentPicker, { DocumentPickerUtil } from 'react-native-document-picker';
import FileViewer from 'react-native-file-viewer';
import RNFetchBlob from 'rn-fetch-blob';

const today = new Date();
const date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
const time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();

const SocketApiTesting = (props) => {
  const [chatMessage, setMessage] = useState('')
  const [chatMessages, setMessages] = useState('')
  const [id, setId] = useState(1)
  const [socket, setSocket] = useState(io('http://192.168.0.186:3000/'))
  const [filePath, setFilePath] = useState(null);
  const [image, setImage] = useState('');

  const [isPopup, setPopup] = useState(false);
  const [isImagePopup, setImagePopup] = useState(false);
  const [isLoading, setLoading] = useState(true);
  const [multipleFile, setMultipleFile] = useState([]);
  const [document, setDocument] = useState('');




  const togglePopup = () => setPopup(!isPopup);
  const toggleImagePopup = () => setImagePopup(!isImagePopup);
  const handleLoading = () => setLoading(!isLoading)

  useEffect(() => {
    requestCameraPermission()
    socket.on('chat_message', (msg) => {
      setMessages(msg)
      // console.log(msg)

    });
  }, [chatMessages])

  const chooseFile = () => {
    let options = {
      // saveToPhotos: true,
      mediaType: 'mixed',
      includeBase64: true,
      // includeBase64
    };
    launchImageLibrary(options, (response) => {
      console.log('Response = ', response);
      // setUploadSuccessMessage('');
      if (response.didCancel) {
        alert('User cancelled camera picker');
        return;
      } else if (response.errorCode == 'camera_unavailable') {
        alert('Camera not available on device');
        return;
      } else if (response.errorCode == 'permission') {
        alert('Permission not satisfied');
        return;
      } else if (response.errorCode == 'others') {
        alert(response.errorMessage);
        return;
      }
      setFilePath(response);
      setImage(response.assets[0].base64)
    });
  };

  const chooseFileFromCamera = () => {
    let options = {
      // saveToPhotos: true,
      mediaType: 'photo',
      includeBase64: true,
      cameraType: 'front'
    };
    launchCamera(options, (response) => {
      console.log('Response = ', response);
      if (response.didCancel) {
        alert('User cancelled camera picker');
        return;
      } else if (response.errorCode == 'camera_unavailable') {
        alert('Camera not available on device');
        return;
      } else if (response.errorCode == 'permission') {
        alert('Permission not satisfied');
        return;
      } else if (response.errorCode == 'others') {
        alert(response.errorMessage);
        return;
      }
      setFilePath(response);
      setImage(response.assets[0].base64)

    });
  };

  const requestCameraPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA,

        {
          title: "Cool Photo App Camera Permission",
          message:
            "Cool Photo App needs access to your camera " +
            "so you can take awesome pictures.",
          buttonNeutral: "Ask Me Later",
          buttonNegative: "Cancel",
          buttonPositive: "OK"
        }
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log("You can use the camera");
      } else {
        console.log("Camera permission denied");
      }
    } catch (err) {
      console.warn(err);
    }
  };


  const submitChatMessage = () => {

    let param = {
      message: chatMessage,
      id: id,
      image: image,
      time: time,
      document: document,
    }
    console.log('++++++++++++++++++++++++++++++++++++++++++++++++++++');
    console.log(param);
    handleLoading(true)
    socket.emit('chat_message', param);
    handleLoading(false)
    setFilePath(null);
    setMultipleFile(param.document);
    setImage('')
  }


  const selectMultipleFile = async () => {

    //Opening Document Picker for selection of multiple file
    try {
      const results = await DocumentPicker.pickMultiple({
        type: [DocumentPicker.types.allFiles],
        //There can me more options as well find above
      });

      var formdata = new FormData();
      // formdata.append("file_url", fileInput.files[0], "test 3.pdf");
      formdata.append("file_url", results[0], "test 3.pdf");


      var requestOptions = {
        method: 'POST',
        body: formdata,
        redirect: 'follow'
      };

      fetch("https://demoapps.in/poker/index.php/Image_controller/upload_file", requestOptions)
        .then(response => response.json())
        .then(result => {
          console.log(result.poker.file_url)
          setDocument(result.poker.file_url)
        })
        .catch(error => console.log('error', error));

      setMultipleFile(results);
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        alert('Canceled from multiple doc picker');
      } else {
        alert('Unknown Error: ' + JSON.stringify(err));
        throw err;
      }
    }




  }


  const renderItem = (item, index) => {
    // console.log(item.item.id);

    if (item.item.image != '' && item.item.message != '') {
      return (
        <View style={{
          alignItems: item.item.id == id ? 'flex-end' : 'baseline', margin: 5,

        }} >

          <TouchableOpacity
            style={{
              alignItems: item.item.id == id ? 'flex-end' : 'baseline', margin: 5,
              // marginTop: 7,
              backgroundColor: '#005c49',
              borderRadius: 14,
              borderRadius: 10,
            }}
            onPress={setImagePopup}>

            <ImageBackground
              borderRadius={10}
              source={{ uri: 'data:image/png;base64,' + item.item.image }}
              style={{
                height: 290,
                width: 248,
                borderColor: '#005c49',
                justifyContent: 'flex-end',
                alignItems: 'flex-end',

              }}
              imageStyle={{
                borderWidth: 2.5,
                borderColor: 'green'
              }}
            >

            </ImageBackground>
            <View style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              width: 250,
              backgroundColor: item.item.id == id ? '#005c49' : '#202a33',
              paddingLeft: 7,
              paddingBottom: 6,
              borderRadius: 10
            }}>
              <Text style={{ fontSize: 16, color: 'white' }}>
                {item.item.message}
              </Text>

              <Text style={{ fontSize: 10, color: 'white', marginTop: 10 }} >{` ` + item.item.time} </Text>

            </View>
          </TouchableOpacity>
          {multipleFile != '' ?

            <TouchableOpacity
              onPress={() => {
                FileViewer.open((multipleFile[0].fileCopyUri))
              }}
              style={{
                paddingHorizontal: 6,
                paddingVertical: 5,
                backgroundColor: item.item.id == id ? '#005c49' : '#202a33',
                borderRadius: 12,
                marginTop: 8,
                alignSelf: 'flex-end',
                marginHorizontal: 3
              }}>
              <TouchableOpacity
                onPress={() => {
                  FileViewer.open((multipleFile[0].fileCopyUri))  //file viewer
                }}
                style={{
                  backgroundColor: '#1b4a2d',
                  flexDirection: 'row',
                  borderRadius: 6,
                  alignItems: 'center',
                }}>
                <View style={{ paddingVertical: 5, paddingHorizontal: 2, borderRadius: 2, backgroundColor: multipleFile[0].type == "application/pdf" ? 'red' : 'gray' }}>
                  <Text style={{ fontSize: 13, color: 'white', }}>{multipleFile[0].type.split('/').slice(1)} </Text>
                </View>
                <Text style={{ fontSize: 14, color: 'white', marginLeft: 10 }} >{multipleFile[0].name} </Text>

              </TouchableOpacity>
              <View style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginTop: 12
              }}>
                <Text style={{ fontSize: 10, color: 'white', }} >{multipleFile[0].size} </Text>
                <Text style={{ fontSize: 10, color: 'white', textAlign: 'center' }} >{`   .   ` + multipleFile[0].type} </Text>
                <Text style={{ fontSize: 10, color: 'white', marginLeft: 30 }} >{` ` + item.item.time} </Text>
              </View>
            </TouchableOpacity>
            : null}

        </View>
      )

    } else {
      return (
        <View
          style={{ alignItems: item.item.id == id ? 'flex-end' : 'baseline', margin: 5 }}
        >
          {item.item.image ?
            <ImageBackground
              borderRadius={10}
              source={{ uri: 'data:image/png;base64,' + item.item.image }}
              style={{
                height: 290,
                width: 248,
                borderColor: '#005c49',
                justifyContent: 'flex-end',
                alignItems: 'flex-end',

              }}
              imageStyle={{
                borderWidth: 2.5,
                borderColor: 'green'
              }}
            >
              <Text
                style={{
                  fontSize: 14,
                  paddingVertical: 5, marginRight: 5, color: 'white',
                }}
              >  {` ` + item.item.time} </Text>
            </ImageBackground> : null}

          {item.item.message != '' ?
            <TouchableOpacity onPress={() => alert()}>
              <View style={{
                flexDirection: 'row',
                justifyContent: 'space-around',
                alignItems: 'center',
                backgroundColor: item.item.id == id ? '#005c49' : '#202a33',
                borderRadius: 10,
                paddingVertical: 12,
                paddingHorizontal: 20,
              }}>
                <Text style={{ fontSize: 16, color: 'white', paddingHorizontal: 14 }}>
                  {item.item.message}
                </Text>

                <Text style={{ fontSize: 10, color: 'white', marginTop: 10, alignSelf: 'flex-end' }} >{` ` + item.item.time} </Text>
              </View>
            </TouchableOpacity> : null}

          {multipleFile != '' ?
            <TouchableOpacity
              onPress={() => {
                FileViewer.open((multipleFile[0].fileCopyUri))  //file viewer
              }}
              style={{
                paddingHorizontal: 6,
                paddingVertical: 5,
                backgroundColor: item.item.id == id ? '#005c49' : '#202a33',
                borderRadius: 12,
                marginTop: 8,
                alignSelf: 'flex-end',
                marginHorizontal: 3
              }}>
              <TouchableOpacity
                onPress={() => {
                  FileViewer.open((multipleFile[0].fileCopyUri))  //file viewer
                }}
                style={{
                  backgroundColor: '#1b4a2d',
                  flexDirection: 'row',
                  borderRadius: 6,
                  justifyContent: 'space-between',
                  alignItems: 'center'
                }}>
                <View style={{ paddingVertical: 5, paddingHorizontal: 2, borderRadius: 2, backgroundColor: multipleFile[0].type == "application/pdf" ? 'red' : 'gray' }}>
                  <Text style={{ fontSize: 13, color: 'white', }}>{multipleFile[0].type.split('/').slice(1)} </Text>
                </View>
                <Text style={{ fontSize: 14.5, fontWeight: '500', color: 'white', marginLeft: 10 }} >{multipleFile[0].name} </Text>
              </TouchableOpacity>

              <View style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginTop: 12

              }}>
                <Text style={{ fontSize: 10, color: 'white', }} >{multipleFile[0].size} </Text>
                <Text style={{ fontSize: 10, color: 'white', textAlign: 'center' }} >{`   .   ` + multipleFile[0].type} </Text>
                <Text style={{ fontSize: 10, color: 'white', marginLeft: 30 }} >{` ` + item.item.time} </Text>
              </View>
            </TouchableOpacity>
            : null}


        </View>
      )
    }
  }
  // console.log('document log');
  // console.log(multipleFile);
  // // alert(multipleFile[0].name)
  // console.log('doc log');
  return (

    <View style={styles.container} >

      <View style={{ backgroundColor: '#202a33', borderRadius: 9, marginTop: 7, paddingHorizontal: 14, paddingVertical: 5, alignSelf: 'center', justifyContent: 'center' }}>
        <Text style={{ fontSize: 14, fontWeight: '700', textAlign: 'center', color: 'white' }}>{date} </Text>
      </View>

      <FlatList
        data={chatMessages}
        renderItem={renderItem}
        keyExtractor={(item, index) => 'key' + index}
        extraData={chatMessages}
        bounces={false}
      // inverted

      />

      <View style={{
        flexDirection: 'row',
        width: '95%',
        borderRadius: 100,
        alignSelf: 'center',
        alignItems: 'center',
      }}>


        <View style={{
          flexDirection: 'row',
          width: '85%',
          elevation: 10,
          backgroundColor: '#334657',
          // elevation: 12,
          borderRadius: 100,
          alignItems: 'center',
          marginBottom: 10,
          height: 45,
        }}>
          <TextInput
            placeholder="Message"
            style={{ flex: 1, fontSize: 18, paddingHorizontal: 10 }}
            autoCorrect={false}
            value={chatMessage}
            // value={chatMessage + (multipleFile[0].name)}
            placeholderTextColor="white"
            color="white"
            onChangeText={chatMessage => {
              setMessage(chatMessage)
              // setMultipleFile(multipleFile[0].name)
            }}
          />

          <TouchableOpacity
            style={{
              height: 40, width: 40, justifyContent: 'center',
              alignItems: 'center', borderRadius: 100
            }}
            onPress={togglePopup}>
            <Image
              style={{ height: 28, width: 28, tintColor: 'white' }}
              source={require('../../assets/share.png')}
            />
          </TouchableOpacity>
        </View>


        <TouchableOpacity
          style={{
            height: 40, width: 40, justifyContent: 'center',
            backgroundColor: '#09a587',
            alignItems: 'center', borderRadius: 100,
            marginBottom: 5, marginLeft: 10
          }}
          onPress={submitChatMessage}>
          <Image
            style={{ height: 28, width: 28, tintColor: 'white' }}
            source={require('../../assets/send.png')}
          />
        </TouchableOpacity>
      </View>

      {isPopup ?
        <View style={[{
          flexDirection: 'row',
          justifyContent: 'space-around',
          backgroundColor: '#25343b',
          paddingHorizontal: 15,
          paddingVertical: 13,
          paddingBottom: 30,
          elevation: 6,
          borderBottomWidth: 1
          // height: Dimensions.get('window').height / 3
        }]} >

          <TouchableOpacity
            onPress={() => selectMultipleFile()}
            style={{ justifyContent: 'center', borderRadius: 100, alignItems: 'center', height: 50, width: 50, backgroundColor: '#7e65ff' }} >
            <Image
              style={{ height: 25, width: 25, tintColor: 'white' }}
              source={require('../../assets/document.png')}
            />
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => chooseFileFromCamera()}
            style={{ justifyContent: 'center', borderRadius: 100, alignItems: 'center', height: 50, width: 50, backgroundColor: '#fd2e74' }} >
            <Image
              style={{ height: 25, width: 25, tintColor: 'white' }}
              source={require('../../assets/camera.png')}
            />
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => chooseFile()}
            style={{ justifyContent: 'center', borderRadius: 100, alignItems: 'center', height: 50, width: 50, backgroundColor: '#b85ae4' }} >
            <Image
              style={{ height: 25, width: 25, tintColor: 'white' }}
              source={require('../../assets/gallery.png')}
            />
          </TouchableOpacity>
        </View>
        : null
      }



    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    // height: 400,
    flex: 1,
    backgroundColor: '#25343b',
    // backgroundColor: 'white',

  },
});

export default SocketApiTesting;