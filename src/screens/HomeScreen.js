import React from 'react'
import { Component } from "react";
import { Text, TouchableOpacity, View } from "react-native";
import Footer from '../components/Footer';

class HomeScreen extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (

      <View style={{ flex: 1 }}>
        <View style={{ flex: 1 }} >

        <TouchableOpacity
            onPress={() => { this.props.navigation.navigate('OpenFile') }}
            style={{ width: '90%', backgroundColor: "white", elevation: 5, borderRadius: 10, alignSelf: 'center', justifyContent: "center", alignItems: "center", padding: 10, marginTop: 30 }} >
            <Text>
              {'Open File'}
            </Text>
          </TouchableOpacity>

        <TouchableOpacity
            onPress={() => { this.props.navigation.navigate('PickLocalFile') }}
            style={{ width: '90%', backgroundColor: "white", elevation: 5, borderRadius: 10, alignSelf: 'center', justifyContent: "center", alignItems: "center", padding: 10, marginTop: 30 }} >
            <Text>
              {'Document Picker'}
            </Text>
          </TouchableOpacity>
        
        <TouchableOpacity
            onPress={() => { this.props.navigation.navigate('SocketApiScreen') }}
            style={{ width: '90%', backgroundColor: "white", elevation: 5, borderRadius: 10, alignSelf: 'center', justifyContent: "center", alignItems: "center", padding: 10, marginTop: 30 }} >
            <Text>
              {'Socket Api'}
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => { this.props.navigation.navigate('WeatherForcasting') }}
            style={{ width: '90%', backgroundColor: "white", elevation: 5, borderRadius: 10, alignSelf: 'center', justifyContent: "center", alignItems: "center", padding: 10, marginTop: 30 }} >
            <Text>
              {'Weather Forcasting'}
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => { this.props.navigation.navigate('LazyLoadingScreen') }}
            style={{ width: '90%', backgroundColor: "white", elevation: 5, borderRadius: 10, alignSelf: 'center', justifyContent: "center", alignItems: "center", padding: 10, marginTop: 30 }} >
            <Text>
              {'Lazy Loading'}
            </Text>
          </TouchableOpacity>


          <TouchableOpacity
            onPress={() => { this.props.navigation.navigate('VideoRecordingScreen') }}
            style={{ width: '90%', backgroundColor: "white", elevation: 5, borderRadius: 10, alignSelf: 'center', justifyContent: "center", alignItems: "center", padding: 10, marginTop: 30 }} >
            <Text>
              {'Video Recording'}
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => { this.props.navigation.navigate('WeatherForcasting') }}
            style={{ width: '90%', backgroundColor: "white", elevation: 5, borderRadius: 10, alignSelf: 'center', justifyContent: "center", alignItems: "center", padding: 10, marginTop: 30 }} >
            <Text>
              {'Grid View'}
            </Text>
          </TouchableOpacity>

        </View>
        <Footer
          navigation={this.props.navigation}
        />
      </View>
    )
  }
}

export default HomeScreen