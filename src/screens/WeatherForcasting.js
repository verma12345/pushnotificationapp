import React, { Component } from 'react';
import { FlatList, View, Text, PermissionsAndroid } from 'react-native';
import ForecastCard from '../components/ForecastCard';
import Geolocation from '@react-native-community/geolocation';

export default class WeatherForcasting extends Component {

    constructor(props) {
        super(props);

        this.state = {
            latitude: 0,
            longitude: 0,
            forecast: [],
            error: '',
            speed: 0
        };
        this.interval = null
    }

    requestLocationPermission = async () => {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                {
                    'title': 'Example App',
                    'message': 'Example App access to your location '
                }
            )
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                console.log("You can use the location")
                alert("You can use the location");
            } else {
                console.log("location permission denied")
                alert("Location permission denied");
            }
        } catch (err) {
            console.warn(err)
        }
    }

    componentDidMount() {
        // Get the user's location
        this.requestLocationPermission()
        this.getLocation();
        this.getSpeed()
    }

    getSpeed() {
        this.interval = setInterval(() => {
            console.log('aaaaaaaaaaaaaaaaaaaaaa');
            Geolocation.getCurrentPosition((position) => {
                this.setState({ speed: position.coords.speed })
            },
                (error) => this.setState({ forecast: error.message }),
                { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
            );
        }, 1000);
    }

    componentWillUnmount() {
        clearInterval(this.interval)
    }
    getLocation() {

        // Get the current position of the user
        Geolocation.getCurrentPosition(
            (position) => {
                this.setState(
                    (prevState) => ({
                        latitude: position.coords.latitude,
                        longitude: position.coords.longitude
                    }), () => { this.getWeather(); }
                );
            },
            (error) => this.setState({ forecast: error.message }),
            { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
        );
    }

    getWeather() {

        // Construct the API url to call
        let url = 'https://api.openweathermap.org/data/2.5/forecast?lat=' + this.state.latitude + '&lon=' + this.state.longitude + '&units=metric&appid=f4d55630e56461efffa46781348debb0';

        // Call the API, and set the state of the weather forecast
        fetch(url)
            .then(response => response.json())
            .then(data => {
                this.setState((prevState, props) => ({
                    forecast: data
                }));
            })
    }

    renderItem = (data) => {
        return (
            <ForecastCard
                detail={data.item}
                location={this.state.forecast.city.name}
            />
        )
    }
    render() {
        return (
            <View>
                <Text style={{ textAlign: 'center' }} >{this.state.speed + ' km/hr'} </Text>
                <FlatList
                    data={this.state.forecast.list}
                    style={{ marginTop: 20 }}
                    keyExtractor={item => item.dt_txt}
                    renderItem={this.renderItem}
                />
            </View>

        );
    }
}