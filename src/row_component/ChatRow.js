import React from 'react'
import { ImageBackground, Text } from 'react-native'
import { TouchableOpacity, View } from 'react-native'

const ChatRow = (props) => {
    // console.log(props.data_row);

    if (props.data_row.image != '' && props.data_row.message != '' || props.data_row.document.includes('jpg')) {
        return (
            <View style={{
                alignItems: props.data_row.id == props.id ? 'flex-end' : 'baseline', margin: 5,

            }} >

                <TouchableOpacity
                    style={{
                        alignItems: props.data_row.id == props.id ? 'flex-end' : 'baseline', margin: 5,
                        // marginTop: 7,
                        backgroundColor: props.data_row.id == props.id ? '#005C49' : '#202A33',
                        borderRadius: 14,
                        borderRadius: 10,
                    }}
                >

                    <ImageBackground
                        borderRadius={10}
                        source={{ uri: props.data_row.document.includes('jpg') ? props.data_row.document : 'data:image/png;base64,' + props.data_row.image }}
                        style={{
                            height: 290,
                            width: 248,
                            borderColor: props.data_row.id == props.id ? '#005C49' : '#202A33',
                            justifyContent: 'flex-end',
                            alignItems: 'flex-end',
                        }}
                        imageStyle={{
                            borderWidth: 2.5,
                            borderColor: props.data_row.id == props.id ? '#005C49' : '#202A33',
                        }}
                    >

                    </ImageBackground>
                    <View style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        width: 250,
                        backgroundColor: props.data_row.id == props.id ? '#005c49' : '#202a33',
                        paddingLeft: 7,
                        paddingBottom: 6,
                        borderRadius: 10
                    }}>
                        <Text style={{ fontSize: 16, color: 'white' }}>
                            {props.data_row.message}
                        </Text>

                        <Text style={{ fontSize: 10, color: 'white', marginTop: 10 }} >{` ` + props.data_row.time} </Text>

                    </View>
                </TouchableOpacity>


            </View>
        )

    }
    else if (props.data_row.document != '' && props.data_row.message != '' && props.data_row.document.includes('.pdf')) {
        return (
            <View style={{
                alignItems: props.data_row.id == props.id ? 'flex-end' : 'baseline', margin: 5,

            }} >
                <TouchableOpacity
                    onPress={() => props.onPermission(props.data_row.document)}
                    style={{
                        paddingHorizontal: 6,
                        paddingVertical: 8,
                        backgroundColor: props.data_row.id == props.id ? '#005c49' : '#202a33',
                        borderRadius: 12,
                        marginTop: 8,
                        alignSelf: 'flex-end',
                        marginHorizontal: 3,
                        borderRadius: 10
                    }}>
                    <TouchableOpacity
                        onPress={() => { props.onPermission(props.data_row.document) }}
                        style={{
                            backgroundColor: props.data_row.id == props.id ? '#005C49' : '#202A33',
                            flexDirection: 'row',
                            borderRadius: 6,
                            alignItems: 'center',
                        }}>
                        <View style={{ paddingVertical: 5, paddingHorizontal: 2, borderRadius: 2, backgroundColor: props.data_row.document.includes('.pdf') ? 'red' : 'gray' }}>
                            <Text style={{ fontSize: 13, color: 'white', }}>{props.data_row.document.split('/').slice(1)} </Text>
                        </View>
                        {/* <Text style={{ fontSize: 14, color: 'white', marginLeft: 10 }} >{props.data_row.document[0].name} </Text> */}

                    </TouchableOpacity>
                    <View style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                        marginBottom: 8,
                        marginTop: 8
                    }}>
                        {/* <Text style={{ fontSize: 10, color: 'white', }} >{props.data_row.document.size} </Text> */}
                        <Text style={{ fontSize: 16, color: 'white', paddingHorizontal: 14, }}>
                            {props.data_row.message}
                        </Text>
                        <Text style={{ fontSize: 10, color: 'white', marginLeft: 30 }} >{` ` + props.data_row.time} </Text>
                    </View>
                </TouchableOpacity>

            </View>
        )
    }
    else {
        return (
            <View
                style={{ alignItems: props.data_row.id == props.id ? 'flex-end' : 'baseline', margin: 5 }}
            >

                {props.data_row.image ?
                    <ImageBackground
                        borderRadius={10}
                        source={{ uri: 'data:image/png;base64,' + props.data_row.image }}
                        style={{
                            height: 290,
                            width: 248,
                            borderColor: props.data_row.id == props.id ? '#005C49' : '#202A33',
                            justifyContent: 'flex-end',
                            alignItems: 'flex-end',

                        }}
                        imageStyle={{
                            borderWidth: 2.5,
                            borderColor: props.data_row.id == props.id ? '#005C49' : '#202A33',
                        }}
                    >
                        <Text
                            style={{
                                fontSize: 14,
                                paddingVertical: 5, marginRight: 5, color: 'white',
                            }}
                        >  {` ` + props.data_row.time} </Text>
                    </ImageBackground> : null}

                {props.data_row.message != '' ?
                    <TouchableOpacity
                    // onPress={() => { }}
                    >
                        <View style={{
                            flexDirection: 'row',
                            justifyContent: 'space-around',
                            alignItems: 'center',
                            backgroundColor: props.data_row.id == props.id ? '#005c49' : '#202a33',
                            borderRadius: 10,
                            paddingVertical: 12,
                            paddingHorizontal: 20,
                        }}>
                            <Text style={{ fontSize: 16, color: 'white', paddingHorizontal: 14 }}>
                                {props.data_row.message}
                            </Text>

                            <Text style={{ fontSize: 10, color: 'white', marginTop: 10, alignSelf: 'flex-end' }} >{` ` + props.data_row.time} </Text>
                        </View>
                    </TouchableOpacity> : null}

                {props.data_row.document != '' ?

                    <View
                        style={{ alignItems: props.data_row.id == props.id ? 'flex-end' : 'baseline', margin: 5 }}
                    >

                        <TouchableOpacity
                            onPress={() => { props.onPermission(props.data_row.document) }}
                            style={{
                                paddingHorizontal: 6,
                                paddingVertical: 5,
                                backgroundColor: props.data_row.id == props.id ? '#005c49' : '#202a33',
                                borderRadius: 12,
                                marginTop: 8,
                                alignSelf: 'flex-end',
                                marginHorizontal: 3
                            }}>
                            <TouchableOpacity
                                onPress={() => { props.onPermission(props.data_row.document) }}
                                style={{
                                    backgroundColor: '#1b4a2d',
                                    flexDirection: 'row',
                                    borderRadius: 6,
                                    justifyContent: 'space-between',
                                    alignItems: 'center'
                                }}>
                                <View style={{ paddingVertical: 5, paddingHorizontal: 2, borderRadius: 2, backgroundColor: props.data_row.document.includes('.pdf') ? 'red' : 'gray' }}>
                                    {/* <Text style={{ fontSize: 13, color: 'white', }}>{props.data_row.document.split('/').slice(1) } </Text> */}
                                    <Text style={{ fontSize: 13, color: 'white', }}>{"pdf"} </Text>
                                </View>
                                {/* <Text style={{ fontSize: 14.5, fontWeight: '500', color: 'white', marginLeft: 10 }} >{props.data_row.document[0].name} </Text> */}
                            </TouchableOpacity>

                            <View style={{
                                flexDirection: 'row',
                                alignItems: 'center',
                                marginTop: 12

                            }}>
                                {/* <Text style={{ fontSize: 10, color: 'white', }} >{filesize(props.data_row.document[0].size)} </Text> */}
                                {/* <Text style={{ fontSize: 10, color: 'white', textAlign: 'center' }} >{`   .   ` +  props.data_row.document.split('/').slice(1) } </Text> */}
                                <Text style={{ fontSize: 10, color: 'white', marginLeft: 30 }} >{` ` + props.data_row.time} </Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    : null}


            </View>
        )
    }
}

export default ChatRow