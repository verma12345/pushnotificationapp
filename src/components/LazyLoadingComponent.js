import React from 'react'
import { ScrollView, StyleSheet, Text, TouchableOpacity, View } from "react-native"
import SkeletonPlaceholder from 'react-native-skeleton-placeholder'

const LazyLoadingComponent = (props) => {
    return (
        <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{}} >

            <SkeletonPlaceholder
                // backgroundColor='lightblue'
                // highlightColor='white'
                speed={2000}
                direction='right' >
                <View style={{ flexDirection: "row", alignItems: "center", marginVertical:10,}}>
                    <View style={{ width: 60, height: 60, borderRadius: 50 }} />
                    <View style={{ marginLeft: 20 }}>
                        <View style={{ width: 120, height: 20, borderRadius: 4 }} />
                        <View style={{ marginTop: 6, width: 200, height: 20, borderRadius: 4 }} />
                    </View>
                </View>
                <View style={{ width: '100%', height: 20, }} />
                <View style={{ width: '100%', height: 200,marginTop:10 }} />
            </SkeletonPlaceholder>
            <SkeletonPlaceholder
                speed={2000}
                direction='right' >
                <View style={{ flexDirection: "row", alignItems: "center", marginVertical:10,}}>
                    <View style={{ width: 60, height: 60, borderRadius: 50 }} />
                    <View style={{ marginLeft: 20 }}>
                        <View style={{ width: 120, height: 20, borderRadius: 4 }} />
                        <View style={{ marginTop: 6, width: 200, height: 20, borderRadius: 4 }} />
                    </View>
                </View>
                <View style={{ width: '100%', height: 20, }} />
                <View style={{ width: '100%', height: 200,marginTop:10 }} />
            </SkeletonPlaceholder>

            <SkeletonPlaceholder
                speed={2000}
                direction='right' >
                <View style={{ flexDirection: "row", alignItems: "center", marginVertical:10,}}>
                    <View style={{ width: 60, height: 60, borderRadius: 50 }} />
                    <View style={{ marginLeft: 20 }}>
                        <View style={{ width: 120, height: 20, borderRadius: 4 }} />
                        <View style={{ marginTop: 6, width: 200, height: 20, borderRadius: 4 }} />
                    </View>
                </View>
                <View style={{ width: '100%', height: 20, }} />
                <View style={{ width: '100%', height: 200,marginTop:10 }} />
            </SkeletonPlaceholder>
            
        </ScrollView>

    )
}

const styles = StyleSheet.create({
    btnStyle: { flex: 1, justifyContent: 'center', alignItems: 'center' }
})

export default LazyLoadingComponent