import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from "react-native"

const Footer = (props) => {
    return (
        <View style={{ height: 50, backgroundColor: 'white', flexDirection: 'row' }} >
            <TouchableOpacity onPress={() => { props.navigation.navigate('HomeScreen') }}
                style={styles.btnStyle} >
                <Text> {"Home"}  </Text>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => { props.navigation.navigate('LazyLoadingScreen') }}
                style={styles.btnStyle} >
                <Text> {"Lazy Loading"}  </Text>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => { props.navigation.navigate('MaskViewScreen') }}
                style={styles.btnStyle} >
                <Text> {"Mask View"}  </Text>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => { props.navigation.navigate('VideoPlayerScreen') }}
                style={styles.btnStyle} >
                <Text> {"Video"}  </Text>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => { props.navigation.navigate('TestVideoPlayer') }}
                style={styles.btnStyle} >
                <Text> {"Youtube"}  </Text>
            </TouchableOpacity>



        </View>
    )
}

const styles = StyleSheet.create({
    btnStyle: { flex: 1, justifyContent: 'center', alignItems: 'center' }
})

export default Footer